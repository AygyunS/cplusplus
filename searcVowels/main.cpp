#include <iostream>
#include<string.h>
#include<cstdio>

using namespace std;

unsigned int vowels (char *s)
{
    unsigned int i, br=0;
    int l = strlen(s);
    for(i=0;i<l-1;i++)
    {
        switch (s[i]){
        case 'a':
        case 'A':
        case 'e':
        case 'E':
        case 'o':
        case 'O':
        case 'i':
        case 'I':
        case 'u':
        case 'U':br++;
        }
    }
    return br;
}

int main()
{
    cout << "Enter sentence: ";
    char sen[100];
    gets(sen);
    cout <<"vowels: "<<vowels(sen)<<endl;
    return 0;
}
