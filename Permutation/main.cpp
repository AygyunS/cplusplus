//Permutation
#include <stdio.h>
#include <iostream>
#define MAXN 100

using namespace std;

const unsigned n = 5;
unsigned char used[MAXN];
unsigned mp[MAXN];
unsigned counter;

void print(void)
{
    unsigned j;
    for (j=0; j<n; j++) {
        cout <<  mp[j]+1 << " ";
    }
    counter++;
    cout << endl;
    cout << counter << endl;
}
void permute(unsigned i)
{
    unsigned j;
    if (i>=n) {
        print(); return;
    }
    for (j=0; j<n; j++)
    {
        if (!used[j])
        {
            used[j]=1; mp[i]=j;
            permute(i+1);
            used[j]=0;
        }
      }
}

int main()
{
    unsigned j;
    for (j=0; j<n; j++){
        used[j]=0;
    }
    permute(0);
    return 0;
}
