#include <stdio.h>
#include <iostream>

using namespace std;

int main(void)
{
    cout << "Vavedete chislo: ";
    unsigned n;
    cin >> n;
    unsigned how, i, j;
    cout << n << " = ";
    i = 1;
    while (n != 1)
    {
        i++;
        how = 0;
        while (n%i==0)
        {
            how++;
            n = n / i;
        }
        for (j = 0; j < how; j++)
            printf("%u ", i);
    }
    cout << endl;

    return 0;
}
