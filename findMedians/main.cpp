#include <iostream>
#include<math.h>

using namespace std;

double findMedian(double a, double b, double c)
{
       return sqrt(2*c*c+2*b*b-a*a)/2;
}

int main()
{
    double a,b,c,m1,m2,m3,ma,mb,mc;
again:
    cout << "Enter a, b, c: \na= ";
    cin >> a;
    cout << "b= ";
    cin >> b;
    cout << "c= ";
    cin >> c;
    if (a<0 || b<0 || c<0)
    {
        cout << "Wrong! Try again." << endl << endl;
        goto again;
    }
    if (a+b<=c || a+c<=b || b+c<=a)
    {
        cout<<"Wrong! Try again."<< endl << endl;
        goto again;
    }
    else
    {
        m1=findMedian(a,b,c);
        m2=findMedian(b,a,c);
        m3=findMedian(c,b,a);

        ma=findMedian(m1,m2,m3);
        mb=findMedian(m2,m1,m3);
        mc=findMedian(m3,m1,m2);
        cout << "Medians are: " << endl;
        cout<<"mA="<<ma << endl;
        cout<<"mB="<<mb << endl;
        cout<<"mC="<<mc << endl;
    }
    return 0;
}
