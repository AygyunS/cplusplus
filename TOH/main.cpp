#include <iostream>
//tower of Hanoi program
using namespace std;

void TOH(int n, int a, int b){
/*
    Assuming a as source stack numbered as 1, b as spare stack numbered as 2 and  c as target
    stack numbered as 3. So once we know values of a and b,
    we can determine c as there sum is a constant number (3+2+1=)6.
*/
    int c = 6-a-b;
    if(n==1){
        cout<<"Move from "<<a<<" to "<<b<<"\n";
    }
    else{
    // Move n-1 disks from 1st to 2nd stack. As we are not allowed to move more than one disks at a time, we do it by recursion. Breaking the problem into a simpler problem.
    TOH(n-1, a, c);
    // Move the last alone disk from 1st to 3rd stack.
    TOH(1, a, b);
    // Put n-1 disks from 2nd to 3rd stack. As we are not allowed to move more than one disks at a time, we do it by recursion. Breaking the problem into a simpler problem.
    TOH(n-1, c, b);
}
}
int main(){
    TOH(2, 1, 3);
    cout<<"FINISHED\n";
    TOH(3, 1, 3);
    cout<<"FINISHED\n";
    TOH(4, 1, 3);
    return 0;
}
