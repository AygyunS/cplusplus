//random number in array..sort!
#include <iostream>
#include <ctime>
#include <stdlib.h>
#include <algorithm>

using namespace std;

void fillFunc(int arr[])
{
    for (int i = 1; i <= 25; i++)
    {
        arr[i] = rand()%100;
    }
}
void printFunc(int arr[])
{
    for (int i = 1; i <= 25; i++)
    {
        cout << arr[i] << " ";
    }
}

int main()
{
    int random[25]; //0-24 is 25 remember array indexes
    int srand((unsigned)time(NULL));
    fillFunc(random);
    printFunc(random);
    cout << endl;
    sort(random, random+25, greater<int>());
    reverse(random, random+25);
    printFunc(random);
    return 0;
}
