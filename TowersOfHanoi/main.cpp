#include <iostream>
using namespace std;
int main()
{
    int n, i;
    restart:
    cout << "Vavedete broq na diskovete:";
    cin >> n;
    if(n>1){
        for (i=1; i < (1 << n); i++){
            cout << "N= "<< i << "|\t"<< ((i&i-1)%3)+1 << " --->  " <<  (((i|i-1)+1)%3)+1 << endl;
        }
    }
    else{
        cout << "Greshen hvod!" << endl << "Opitaite otnovo..." << endl;
        goto restart;
    }
    return 0;
}
