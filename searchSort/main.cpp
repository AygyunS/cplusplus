#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

int show(int random)
{
    cout << random << " | ";
    return 0;
}

void EnterArray(int *arr,int n){
    for(int i=0;i<n;i++){
        arr[i] = n-i;
    }
}

void PrintArray(int *arr,int n){
    for(int i=0;i<n;i++)
        cout<<arr[i]<<" ";
    cout<<endl;
}

int BubbleSort(int *arr,int n, int key){
    for(int i=0;i<n-1;i++) {
		if(arr[i] == key){
			return 1;
		}
    }

    return 0;
}

int BinSearch(int *arr, int n, int key){
		int l = 0, r = n-1;
		while(l<=r){
			int m=(l+r)/2;
			if(arr[m]==key)
				return m+1;
			else if(arr[m] < key)
				l=m+1;
			else
				r=m-1;
		}
		return 0;
}
int SelectSort(int *arr, int n){
    for(int i =0; i<n-1; i++){
    int indMin = i+1;
    for(int j = i + 2; j < n;j++){
        if(arr[j]<arr[indMin]){
            indMin = j;
        }
    }
    swap(arr[i], arr[indMin]);
    }

}

void Sift(int arr[], int v, int r)
{
    int i = v, j=2*i+1;
    int x = arr[i];
    if(j<r && arr[j+1] > arr[j]){
        j++;
    }
    while(j<=r && arr[j]>x){
        arr[i] = arr[j];
        i=j;
        j=2*i+1;
        if(j<r && arr[j+1]>arr[j]){
            j+1;
        }
    }
    if(arr[i]!=x){
        arr[i]=x;
    }
}

void HeapSort(int *arr, int n){
    for(int i = n/2;i>=0;i--)
    {
        Sift(arr,i,n-1);

    }
    int m = n-1;
    while(m>0)
    {
        swap(arr[0], arr[m]);
        m--;
        Sift(arr, 0, m);
    }
}

void InsertSort(int *arr,int n){
    for(int i=1;i<n;i++){
        int x=arr[i],j=i;
        while(j>0 && arr[j-1]>x){
            arr[j]=arr[j-1];
            j--;
        }
        arr[j]=x;
    }
}

void insertion_bsearch(int *arr, int n)
{
    for(int i = 1; i<n;i++){
        int x = arr[i];
        int j = 0;
        int el = 0;
        int r = i-1;
        int med = 0;
        while(el <= r){
            med = (el+r)/2;
            if(x < arr[med]){
                r = med-1;

            }
            else {
                el = med+1;
            }
        }
        for(j=i-1; j>=el;j--){
            arr[j+1] = arr[j];
        }
        arr[el] = x;
    }
}

int main()
{
    const int N=10;
	int key;
	cout << "Enter number: ";
	cin >> key;
    int arr[N];
    EnterArray(arr,N);
    InsertSort(arr,N);
	SelectSort(arr, N);
	int pos = BinSearch(arr, N, key);
	PrintArray(arr,N);
    cout << "position: " <<pos << endl;
    insertion_bsearch(arr, N);
    PrintArray(arr,N);

    const int n1 = 10;
    int i = 1;
    int random[n1];
    srand((unsigned)time(NULL));
    for (int i = 0; i < n1; i++)
    {
        random[i] = 1+ rand() % 100;
        show (random[i]);
    }
    int key1;
	cout << endl << "Enter another number: ";
	cin >> key1;
	PrintArray(random, n1);
	SelectSort(random, n1);
	insertion_bsearch(random, n1);
    int pos1 = BinSearch(random, n1, key1);

    cout << endl << endl;

    PrintArray(random, n1);
    if(BinSearch(random, n1, key1)==0){
        cout << "This number is not found!!!" << endl;
    }else {
        cout << "Number that you are searching is:   " << key1 <<
         " --- at position: " << pos1 << endl;
    }

    return 0;
}
