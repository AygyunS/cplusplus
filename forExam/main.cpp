#include <iostream>

using namespace std;

void EnterArray(int *arr,int n){
    for(int i=0;i<n;i++){
        arr[i] = n-i;
    }
}

void PrintArray(int *arr,int n){
    for(int i=0;i<n;i++)
        cout<<arr[i]<<" ";
    cout<<endl;
}
void BubbleSort(int *arr, int n)
{
    int i, j, temp;
    for(i=1;i<n;++i)
    {
        for(j=0;j<(n-i);++j)
            if(arr[j]>arr[j+1])
            {
                temp=arr[j];
                arr[j]=arr[j+1];
                arr[j+1]=temp;
            }
    }
}
int BinSearch(int *arr, int n, int key){
		int l = 0, r = n-1;
		while(l<=r){
			int m=(l+r)/2;
			if(arr[m]==key)
				return m+1;
			else if(arr[m] < key)
				l=m+1;
			else
				r=m-1;
		}
		return 0;
}

int main()
{
    const int N=100;
	int key;
	cout << "vavedete chisloto: ";
	cin >> key;
    int arr[N];
    EnterArray(arr,N);
    BubbleSort(arr, N);
	int pos = BinSearch(arr, N, key);
	PrintArray(arr,N);
    cout << "poziciq: " <<pos << endl;
    return 0;
}
